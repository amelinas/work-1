var slideIndex = 1;
showSlides(slideIndex);


function plusSlides(n) {
  showSlides(slideIndex += n);
}


function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" dot_active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " dot_active";
} 


function btns__popap(state)
{
document.getElementById('btns__window').style.display = state;
document.getElementById('btns__popap').style.display = state;
}

var panelItem = document.querySelectorAll('.menu-panel__title'),
    textItem = document.querySelectorAll('.menu-panel__post');
    
panelItem.__proto__.forEach = [].__proto__.forEach;

var activePanel;
panelItem.forEach(function(item, i, panelItem) {
  item.addEventListener('click', function(e) {
    
    this.classList.add('panel__active');
    this.nextElementSibling.classList.add('active');
   
    if (activePanel) {
      activePanel.classList.remove('panel__active');
      activePanel.nextElementSibling.classList.remove('active');
    }
    
    activePanel = (activePanel === this) ? 0 : this;
  });
});